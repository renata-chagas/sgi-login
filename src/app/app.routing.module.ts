import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './core/auth.guard';

const appRoutes: Routes = [
  {
    path: 'login',
    loadChildren: 'app/public/login/login.module#LoginModule'
  },
  {
    path: '',
    loadChildren: 'app/public/workspace/workspace.module#WorkspaceModule',
    canLoad: [AuthGuard]
  },
  {
    path: '**',
    redirectTo: 'dashboard'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(appRoutes, { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule {}

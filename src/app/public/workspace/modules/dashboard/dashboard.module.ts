import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { MaterializeModule } from 'angular2-materialize';
import { DashboardComponent } from './dashboard.component';

const components = [DashboardComponent];

@NgModule({
  imports: [
    CommonModule,
    MaterializeModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule
  ],
  declarations: [...components],
  exports: [...components],
  providers: []
})
export class DashboardModule {}

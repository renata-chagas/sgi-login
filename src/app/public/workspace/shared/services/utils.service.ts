import { AuthGuard } from './../../../../core/auth.guard';
import { LoginComponent } from './../../../login/login.component';
import { LoginService } from './../../../login/login.service';
import { SessionService } from './../../../../core/session.service';
import { Injectable } from '@angular/core';
import { Http, Response, RequestOptions, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { Router } from '@angular/router';


declare var Materialize: any;


@Injectable()
export class UtilsService {

  allMessageTypes: any = {
    success: 'green',
    alert: 'yellow darken-3',
    danger: 'red'
  };

  toastMessageToShow: any = {
    html : '',
    classes : '',
    inDuration: null
  };

  constructor(private router: Router) {}

  public showToastMessage(message?: string, messageType?: string,
    timeOut?: number, toastMessageToShow?: any): void {

      if (!!!message || !!!messageType) {
        console.log('Você não informou todos os parâmetros.');
      } else {
        if (message === 'defaultError') {
          message = 'Oops! Algo não está certo. Entre em contato com o GCTI!';
        }

        if (!!!timeOut) {
          timeOut = 2500;
        }

        Materialize.toast(message, timeOut, this.allMessageTypes[messageType]);
      }
  }


  public extractData(res: Response) {
    const body = res.json();
    return body || {};
  }

  public handleError(error: Response | any) {
    let errMsg: string;
    if (error instanceof Response) {
      const body = error.json() || '';
      const err = body.error || JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }

    if (error.status === 401) {
      setTimeout(() => {
        // this.login.logout();
        window.location.href = '/#/login';
        location.reload();
      }, 1000);
    }
    return Observable.throw(errMsg);
  }
}

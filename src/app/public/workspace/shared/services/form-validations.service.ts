import { FormControl } from '@angular/forms';
import { Injectable } from '@angular/core';


@Injectable()
export class FormValidationsService {

    constructor() { }

    static getErrorMessage(fieldName: string, validatorName: string, validatorValue?: any): string {
        const errorMessages: any = {
            'required': `${fieldName} é obrigatório.`,
            'minlength': `${fieldName} precisa ter no mínimo ${validatorValue.requiredLength} caracteres.`,
            'maxlength': `${fieldName} só pode ter no máximo ${validatorValue.requiredLength} caracteres.`,
            'cepInvalido': `Esse CEP não é válido.`,
            'cnpjInvalido': `Esse CNPJ não é válido.`,
            'email': `Formato de email inválido.`,
        };

        return errorMessages[validatorName];
    }
}

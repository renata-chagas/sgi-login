import { AgmCoreModule } from '@agm/core';
import { CommonModule, DatePipe, DecimalPipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { NgSelectModule } from '@ng-select/ng-select';
import { MaterializeModule } from 'angular2-materialize';
import { TextMaskModule } from 'angular2-text-mask';

import { FormValidationsService } from './services/form-validations.service';
import { UtilsService } from './services/utils.service';



const components = [
];

const providers = [
  UtilsService,
  FormValidationsService,
  DatePipe,
  DecimalPipe,
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    NgSelectModule,
    TextMaskModule,
    MaterializeModule,
    RouterModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyBJCeGhoxR3tKt79eArYbSBXGXfy8qe4f8',
      libraries: ['places']
    })
  ],
  declarations: [...components],
  exports: [...components],
  providers: [...providers]
})
export class SharedModule { }

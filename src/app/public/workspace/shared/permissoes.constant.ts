export const PERMISSOES = Object.freeze({
  administrador: 'ADMINISTRADOR_ALMOXARIFADO',
  gerente: 'GERENTE_ALMOXARIFADO',
  operador: 'OPERADOR_ALMOXARIFADO',
  atendente: 'ATENDENTE_ALMOXARIFADO'
});

export interface UsuarioToken {
  id: number;
  nome: string;
  matricula: string;
  cpf: string;
  login: string;
  localId: number;
  localNome: string;
  grupoId: number;
  grupoNome: string;
  celulaId: number;
  celulaNome: string;
  tipoDoAlmoxarifado: string;
}

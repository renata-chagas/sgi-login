import { UsuarioToken } from "./usuario-token.model";

export const DEFAULT_USUARIO_TOKEN: UsuarioToken = {
    nome: "",
    celulaId: null,
    celulaNome: "",
    cpf: "",
    grupoId: null,
    grupoNome: "",
    id: null,
    localId: null,
    localNome: "",
    login: "",
    matricula: "",
    tipoDoAlmoxarifado: ""
}
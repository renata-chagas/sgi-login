import { PERMISSOES } from '../permissoes.constant';
import { DadosDeAcessoDeModulo } from './dados-de-acesso-de-modulo.model';
import { UsuarioToken } from './usuario-token.model';

export function getUsuario(): UsuarioToken {
  try {
    return JSON.parse(
      JSON.parse(atob(window.sessionStorage.getItem('jwt').split('.')[1]))
        .values
    );
  } catch (_) {
    return null;
  }
}

export function podeAcessar(
  usuario: UsuarioToken,
  dados?: DadosDeAcessoDeModulo
): boolean {
  if (!dados) {
    return true;
  }

  if (dados.grupos) {
    if (!dados.grupos.includes(usuario.grupoNome)) {
      return false;
    }
  }

  if (dados.tiposDeAlmoxarifado) {
      return dados.tiposDeAlmoxarifado.includes(usuario.tipoDoAlmoxarifado);
  }

  return true;
}

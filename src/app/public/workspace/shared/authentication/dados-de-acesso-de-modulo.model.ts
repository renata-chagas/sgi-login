/**
 * @author Danilo
 * Esta interface modela os dados de acesso informados em cada módulo para que o auth-guard, sidebar e dashboard
 * tenham conhecimento das rotas e módulos disponíveis ao usuário.
 */

export interface DadosDeAcessoDeModulo {
  grupos?: Array<string>;
  tiposDeAlmoxarifado?: Array<string>;
}

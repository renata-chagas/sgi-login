import { Component, EventEmitter, OnInit } from '@angular/core';
import { MaterializeAction } from 'angular2-materialize';
import { getUsuario } from '../authentication/token-utils.model';
import { DEFAULT_USUARIO_TOKEN } from '../authentication/usuario-token.constants';
import { UsuarioToken } from '../authentication/usuario-token.model';

@Component({
  selector: 'header-component',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.sass']
})
export class HeaderComponent implements OnInit {
  modalActions = new EventEmitter<string | MaterializeAction>();
  user: UsuarioToken = DEFAULT_USUARIO_TOKEN;

  constructor() {}

  ngOnInit() {
    this.user = getUsuario();
  }

  closeModal() {
    this.modalActions.emit({ action: 'modal', params: ['close'] });
  }
}

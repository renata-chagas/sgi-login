import { Component, ViewChild } from '@angular/core';
import { Http } from '@angular/http';
import { Router } from '@angular/router';
import { getUsuario } from './shared/authentication/token-utils.model';

@Component({
  selector: 'workspace',
  templateUrl: './workspace.component.html',
  styleUrls: ['./workspace.component.sass']
})
export class WorkspaceComponent {
  data: any;

  notificationIsOpen = false;
  sidebarLinksIsOpen = false;

  totalNotifications = 0;
  notificationId: number;
  notificationDescription: string;

  usuario = getUsuario();
  hasPermission: boolean;

  constructor(
    private router: Router,
    private http: Http
  ) {
    this.hasPermission =
      this.usuario.grupoNome === 'ADMINISTRADOR_ALMOXARIFADO' ||
      this.usuario.grupoNome === 'GERENTE_ALMOXARIFADO' ||
      this.usuario.grupoNome === 'OPERADOR_ALMOXARIFADO';
  }

  ngOnInit() {
    
  }

  toggleSidebarLinks(event) {
    this.sidebarLinksIsOpen = event;
  }
}

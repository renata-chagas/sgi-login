import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgSelectModule } from '@ng-select/ng-select';
import { MaterializeModule } from 'angular2-materialize';
import { DashboardModule } from './modules/dashboard/dashboard.module';
import { FooterComponent } from './shared/footer/footer.component';
import { HeaderComponent } from './shared/header/header.component';
import { SharedModule } from './shared/shared.module';
import { WorkspaceComponent } from './workspace.component';
import { WorkspaceRoutingModule } from './workspace.routing.module';

const modules = [
  CommonModule,
  FormsModule,
  ReactiveFormsModule,
  NgSelectModule,
  WorkspaceRoutingModule,
  DashboardModule,
  SharedModule,
  MaterializeModule,
];

const components = [
  WorkspaceComponent,
  HeaderComponent,
  FooterComponent,
];

@NgModule({
  imports: [...modules],
  declarations: [...components],
  providers: []
})
export class WorkspaceModule {}

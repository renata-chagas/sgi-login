export interface User {
  userName: string;
  password: string;
}
export interface RequestStatusConstant {
  status: number;
  color: string;
  icon: string;
  showLoginAlert?: boolean;
  messages: {
    toast: string;
    loginAlert?: string;
  };
}

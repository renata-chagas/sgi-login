import { Component, EventEmitter, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { MaterializeAction } from 'angular2-materialize';
import { SessionService } from '../../core/session.service';
import { RequestStatusConstant, User } from './login.model';
import { LoginService } from './login.service';

declare var Materialize: any;

const MENSAGENS = {
  0: {
    cor: 'red'
  },
  401: {
    cor: 'orange'
  },
  403: {
    cor: 'orange'
  },
  400: {
    cor: 'red'
  },
  500: {
    cor: 'red'
  },
  200: {
    cor: 'green'
  }
};

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  providers: [LoginService]
})
export class LoginComponent implements OnInit, OnDestroy {
  form: FormGroup;

  actions = new EventEmitter<string | MaterializeAction>();

  isLoading = false;

  user: User = {
    userName: '',
    password: ''
  };

  currentRequestStatusConstant: RequestStatusConstant;

  onlineChanges;
  offlineChanges;
  formChanges;

  shouldDisableLogin = false;
  constructor(
    private loginService: LoginService,
    private sessionService: SessionService,
    private router: Router
  ) {}

  setForm() {
    this.form = new FormGroup({
      username: new FormControl(null, [
        Validators.required,
        Validators.maxLength(28)
      ]),
      password: new FormControl(null, [
        Validators.required,
        Validators.maxLength(28)
      ])
    });
  }

  verificaValidTouched(formControl) {
    const control = this.form.get(formControl);
    return !control.valid && control.dirty;
  }

  applyErrorStyle(formControl: string) {
    return {
      invalid: this.verificaValidTouched(formControl)
    };
  }

  checkFormValidation(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach(control => {
      const input = formGroup.get(control);
      input.markAsDirty();
    });
  }

  cleanForm() {
    this.form.reset();
  }

  authenticateUser() {
    this.setLoginInfo();
    const shouldAuthenticate = this.form.valid && !this.shouldDisableLogin;
    if (shouldAuthenticate) {
      this.isLoading = true;
      this.loginService
        .login(this.user.userName, this.user.password)
        .toPromise()
        .then(
          res => {
            this.login(res);
          },
          error => {
            console.log(error);
            
            Materialize.toast(
              `${
                error.status !== 0
                  ? error.message
                  : 'Não foi possível se comunicar com o servidor'
              }`,
              6000,
              MENSAGENS[error.status].cor
            );
          }
        )
        .then(() => (this.isLoading = false));
    } else {
      this.checkFormValidation(this.form);
    }
  }

  login(authResponse: { jwt: string }) {
    window.sessionStorage.setItem('jwt', authResponse.jwt);
    this.router.navigate(['/']);
    Materialize.toast(
      `Bem-vindo ao Login SGI!`,
      4000,
      MENSAGENS[200].cor
    );
  }

  clearStoredUser() {
    this.sessionService.clean();
    this.loginService.logout();
  }

  handleAuthError(error: { message: string; status: number }) {
    this.setLoginInfo();
    if (error.message !== undefined) {
      this.currentRequestStatusConstant.messages.toast = error.message;
      this.currentRequestStatusConstant.messages.loginAlert = error.message;
    }
    this.isLoading = false;
  }

  setLoginInfo() {
    this.user.userName = this.form.controls.username.value;
    this.user.password = this.form.controls.password.value;
  }

  ngOnInit() {
    this.clearStoredUser();
    this.setForm();
  }

  ngOnDestroy() {}
}

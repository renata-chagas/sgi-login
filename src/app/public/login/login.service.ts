import { Injectable } from '@angular/core';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/throw';

@Injectable()
export class LoginService {
  constructor(private http: Http) {}

  login(username: string, password: string) {
    const headers = new Headers({
      Authorization: btoa(username + ':' + password)
    });  
    const options = new RequestOptions({ headers: headers });

    return this.http
      .post('/autenticacao/logon', null, options)
      .map(this.extractData.bind(this))
      .catch(this.handleError);
  }

  isEmptyObject(obj) {
    return obj && Object.keys(obj).length === 0;
  }

  private extractData(res: Response) {
    const body = res.json();
    return body || {};
  }

  private handleError(error: Response | any) {
    try {
      const jsonError = JSON.parse(error._body);
      return Observable.throw({
        message: jsonError.message,
        status: error.status
      });
    } catch (Error) {
      return Observable.throw({ message: undefined, status: error.status });
    }
  }

  logout(): void {
    sessionStorage.removeItem('currentUser');
  }
}

import { podeAcessar } from './public/workspace/shared/authentication/token-utils.model';

const modules = [
];

export const getModules = usuario => {
  const availableModules = modules
    .map(oneModule => {
      oneModule.active = podeAcessar(usuario);
      return oneModule;
    })
    .filter(oneModule => oneModule.active);

  return availableModules;
};

import { Injectable } from '@angular/core';
import { Router, CanLoad, Route } from '@angular/router';
import { getUsuario, podeAcessar } from '../public/workspace/shared/authentication/token-utils.model';
import { UsuarioToken } from '../public/workspace/shared/authentication/usuario-token.model';
import { DadosDeAcessoDeModulo } from '../public/workspace/shared/authentication/dados-de-acesso-de-modulo.model';

@Injectable()
export class AuthGuard implements CanLoad {

    constructor(
        private router: Router
    ) { }

    canLoad(route: Route) {
        const usuario: UsuarioToken = getUsuario();

        if (!usuario) {
            this.redirecionarParaLogin();
            return false;
        }

        const possuiPermissao = podeAcessar(usuario, <DadosDeAcessoDeModulo> route.data);
        if (!possuiPermissao) {
            this.router.navigate(['/dashboard']);
            return false;
        }
        return true;
    }

    private redirecionarParaLogin(): void {
        this.router.navigate(['/login']);
    }
    
}

import { Injectable } from '@angular/core';
import {
  ConnectionBackend,
  Headers,
  Http,
  Request,
  RequestOptions,
  RequestOptionsArgs
} from '@angular/http';
import { environment } from './../../environments/environment';

@Injectable()
export class HttpInterceptor extends Http {
  constructor(backend: ConnectionBackend, defaultOptions: RequestOptions) {
    super(backend, defaultOptions);
  }

  request(url: string | Request, options?: RequestOptionsArgs) {
    return super.request(url, options);
  }

  get(url: string, options?: RequestOptionsArgs) {
    if (url.includes('viacep')) {
      return super.get(this.getUrlEnvironment(url), {});
    } else {
      return super.get(
        this.getUrlEnvironment(url),
        this.getRequestOptionArgs(options)
      );
    }
  }

  post(url: string, body: string, options?: RequestOptionsArgs) {
    return super.post(
      this.getUrlEnvironment(url),
      body,
      this.getRequestOptionArgs(options)
    );
  }

  put(url: string, body: string, options?: RequestOptionsArgs) {
    return super.put(
      this.getUrlEnvironment(url),
      body,
      this.getRequestOptionArgs(options)
    );
  }

  delete(url: string, options?: RequestOptionsArgs) {
    return super.delete(
      this.getUrlEnvironment(url),
      this.getRequestOptionArgs(options)
    );
  }

  getUrlEnvironment(url: string) {
    return url.startsWith('/') ? environment.origin + url : url;
  }

  private getRequestOptionArgs(
    options?: RequestOptionsArgs
  ): RequestOptionsArgs {
    if (!options) {
      options = new RequestOptions();
    }

    if (!options.headers) {
      options.headers = new Headers();
    }

    options.headers.append('Content-Type', 'application/json;charset=utf-8');

    const jwt = window.sessionStorage.getItem('jwt');
    if (jwt) {
      options.headers.append('Authorization', jwt);
    }
    return options;
  }
}

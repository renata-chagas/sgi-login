import { Injectable } from '@angular/core';

@Injectable()
export class SessionService {
  private jwt: String;

  constructor() { }

  getUser(): any {
    return JSON.parse(
      window.sessionStorage.getItem('currentUser')
    );
  }

  setUser(data: string): void {
    window.sessionStorage.setItem('currentUser', data);
  }

  getJwt(): String {
    return JSON.parse(window.sessionStorage.getItem('jwt'));
  }

  setJwt(jwt: string): void {
    window.sessionStorage.setItem('jwt', jwt);
  }

  clean(): void {
    window.sessionStorage.clear();
  }

}

import { Observable } from 'rxjs/Observable';
import { SessionService } from './session.service';
import { Injectable } from '@angular/core';
import { CanLoad, Router } from '@angular/router';

@Injectable()
export class PermissionGuard implements CanLoad {

    constructor(
        private sessionService: SessionService,
        private route: Router
    ) { }

    canLoad(): Observable<boolean> | boolean {
        return true;
    }
}

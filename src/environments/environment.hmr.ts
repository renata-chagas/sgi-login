export const environment = {
  production: false,
  hmr: true,
  //origin: 'http://10.200.2.149:8080/sgi_almoxarifado/api',
  origin: 'http://localhost:8080/sgi_almoxarifado/api',
  originWS: 'ws://sgialmoxarifado.parnamirim.rn.gov.br/ws'
};

export const environment = {
    production: true,
    hmr: false,
    origin: "http://172.16.0.73:8080/sgi_almoxarifado/api",
    originWS: "ws://sgialmoxarifado.parnamirim.rn.gov.br/ws"
  };
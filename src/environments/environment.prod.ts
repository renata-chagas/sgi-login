export const environment = {
  production: true,
  hmr: false,
  origin: "http://sgialmoxarifado.parnamirim.rn.gov.br:8080/api",
  originWS: "ws://sgialmoxarifado.parnamirim.rn.gov.br:8080/ws"
};
